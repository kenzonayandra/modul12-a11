# Modul12-A11

## Group Member:
* Agastya Kenzo Nayandra - 2006535905<br>
* Muh. Kemal Lathif Galih Putra - 2206081225<br>
* Ramadhan Andika Putra - 2206081976<br>
* Darrel Danadyaksa Poli - 2206081995<br>
* Mario Michael Jeremy Sitanggang - 2206828626<br>

## To Do List:
- [Group] Context Diagram
- [Group] Container DIagram
- [Group] Deployment Diagram
- [Individu] Component Diagram
- [Individu] Code Diagram

Discuss your groups project current architecture. Discuss the context, container and also the deployment diagram. Draw it in your favorite drawing editor. Download an image and add it in to your group project repository. 
Commit and push with message: “1. The current architecture of group …. , the context, container and deployment diagram“.

Discuss your group project architectural risk. Be creative and innovative, imagine that your project is having a great success. Think and discuss what will be the risk of the current architecture. Apply the technique of Risk Storming as presented before. 

Draw the expected new future architecture. Put it in your Readme.md 
Commit and push with message:  “2. The future architecture of group … ” 

Discuss it and the write a well written an concise explanation why the risk storming technique is applied. Write it your Readme.md project repository. 
Commit and push with message:  “3. Explanation of risk storming of group … ” 

### 1. The current architecture of group A11

#### The Context Diagram
- For Authentication<br>
![Alt text](<Context Diagram-1.png>)<br>
- For User<br>
![Alt text](<Context Diagram-2.png>)<br>
- For Admin<br>
![Alt text](<Context Diagram-3.png>)


#### The Container Diagram
- For User<br>
![alt text](<Groups Discussion.png>)<br>
- For Admin<br>
![alt text](<Groups Discussion-2.png>)

#### The Deployment Diagram
![alt text](<Groups Discussion-3.png>)

### 2. The Future Architecture of Group A11
- Improvement architecture pada Context Diagram coba ditingkatkan dengan menambahkan Springboot starter security untuk mengamankan aplikasi dan membatasi akses ke beberapa fitur dari role yang ditentukan.
- Improvement architecture pada Container & Deployment Diagram coba ditingkatkan dengan menambahkan Spring Eureka & Cloud Gateaway untuk Load Balancing & Service Registry yang dilakukan secara otomatis
#### The Context Diagram
![alt text](<Groups Discussion-1_improvement.png>)

#### The Container Diagram
- For User
![alt text](<Groups Discussion-4.png>)

- For Admin
![alt text](<Groups Discussion-5.png>)

#### The Deployment Diagram
![alt text](<Groups Discussion-6.png>)

### 3. Explanation of risk storming of group A11

- Kami melakukan risk assesment secara kolektif dan mendapatkan hasil sebagai berikut
- Hasil Risk Assesment

![alt text](<Groups Discussion-8.png>)

###### Kesimpulan Risk Assesment:

- Service authentication sangat krusial dalam beberapa semua aspek seperti Scalability, Availability, Performance, Security dan Data Integrity, karena sangat berkaitan erat dengan Keamanan data dari User dan ancaman security kedepanya. Kami menganggap service ini memiliki nilai likelihood dan impact yang Medium-High secara keseluruhan.

- Untuk ketiga Service lainya seperti Subscription-Management, Item-Management, dan Rating & Review tidak se-krusial autentication, sehingga kami mengasses ketiga service ini dengan nilai impact yang Medium dan likelihood yang Medium juga.